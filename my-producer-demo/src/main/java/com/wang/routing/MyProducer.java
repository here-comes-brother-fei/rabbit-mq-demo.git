package com.wang.routing;

import com.rabbitmq.client.Channel;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.wang.utils.RabbitMqUtil;




/**
 * @BelongsProject: RabbitMqLearn
 * @BelongsPackage: com.wang
 * @Author: wang fei
 * @CreateTime: 2023-02-03  15:58
 * @Description: TODO rabbitmq-routing模式-direct 生产者
 * 关键动作：
 * 在⽣产者发送消息时指明routing-key
 * 在消费者声明队列和交换机的绑定关系时，指明routing-key
 * @Version: 1.0
 */
public class MyProducer {
    //定义交换机名称
    public  static String EXCHANGE_NAME = "my_routing_exchange";

    public static void main(String[] args) throws Exception {
        Connection connection=RabbitMqUtil.getConnection();
        Channel channel = connection.createChannel();
        //声名交换机
        channel.exchangeDeclare(EXCHANGE_NAME, "direct");
        //发送信息
        for (int i = 0; i < 20; i++) {
            String message = "Hello routing模式-direct!";
            channel.basicPublish(EXCHANGE_NAME, "my_routing_key", null, message.getBytes());
        }
        System.out.println("消息发送成功");
        //断开链接
        channel.close();
        connection.close();
    }

}
