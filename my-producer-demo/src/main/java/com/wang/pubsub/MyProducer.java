package com.wang.pubsub;

import com.rabbitmq.client.Channel;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.wang.utils.RabbitMqUtil;




/**
 * @BelongsProject: RabbitMqLearn
 * @BelongsPackage: com.wang
 * @Author: wang fei
 * @CreateTime: 2023-02-03  15:58
 * @Description: TODO rabbitmq-fanout队列模式生产者
 * @Version: 1.0
 */
public class MyProducer {
    //定义交换机名称
    public  static String EXCHANGE_NAME = "my_fanout_exchange";

    public static void main(String[] args) throws Exception {
        Connection connection=RabbitMqUtil.getConnection();
        Channel channel = connection.createChannel();
        //声名交换机
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        //发送信息
        for (int i = 0; i < 20; i++) {
            String message = "Hello RabbitMq!";
            channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());
        }
        System.out.println("消息发送成功");
        //断开链接
        channel.close();
        connection.close();
    }

}
