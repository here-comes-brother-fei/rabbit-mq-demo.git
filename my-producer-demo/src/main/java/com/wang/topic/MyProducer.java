package com.wang.topic;

import com.rabbitmq.client.Channel;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.wang.utils.RabbitMqUtil;




/**
 * @BelongsProject: RabbitMqLearn
 * @BelongsPackage: com.wang
 * @Author: wang fei
 * @CreateTime: 2023-02-03  15:58
 * @Description: TODO rabbitmq-topics模式 生产者
 * 绑定关系中如果使⽤了product.* ,那么在发送消息时：
 * product.add ok
 * product.del ok
 * product.add.one 不ok
 *
 *绑定关系中如果使⽤了product.#,那么在发送消息时：
 * product.add ok
 * product.add.one ok
 * @Version: 1.0
 */
public class MyProducer {
    //定义交换机名称
    public  static String EXCHANGE_NAME = "my_topic_exchange";

    public static void main(String[] args) throws Exception {
        Connection connection=RabbitMqUtil.getConnection();
        Channel channel = connection.createChannel();
        //声名交换机
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        //发送信息
        String message = "topics模式!";
        channel.basicPublish(EXCHANGE_NAME, "product.add.one", null, message.getBytes());
        System.out.println("消息发送成功");
        //断开链接
        channel.close();
        connection.close();
    }

}
