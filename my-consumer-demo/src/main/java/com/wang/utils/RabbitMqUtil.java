package com.wang.utils;


import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @BelongsProject: RabbitMqLearn
 * @BelongsPackage: com.wang.utils
 * @Author: wang fei
 * @CreateTime: 2023-02-03  18:07
 * @Description: TODO 工具类
 * @Version: 1.0
 */
public class RabbitMqUtil {
    private static ConnectionFactory connectionFactory;
    private static String USER_NAME;
    private static String PASSWORD;
    private static String VIRTUAL_HOST;
    private static String HOST;
    private static int PORT;

    static {
        USER_NAME = "wang";
        PASSWORD = "123456";
        HOST = "192.168.206.130";
        PORT = 5672;
        VIRTUAL_HOST = "/test1";
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(HOST);
        connectionFactory.setPort(PORT);
        connectionFactory.setUsername(USER_NAME);
        connectionFactory.setPassword(PASSWORD);
        connectionFactory.setVirtualHost(VIRTUAL_HOST);
    }

    /**
     * @description: 获得连接对象
     * @method: getConnection
     * @author: wang fei
     * @date: 2023/2/3 18:17:02
     * @param: []
     * @return: com.rabbitmq.client.Connection
     **/
    public static Connection getConnection() throws Exception {
        return connectionFactory.newConnection();
    }
}
