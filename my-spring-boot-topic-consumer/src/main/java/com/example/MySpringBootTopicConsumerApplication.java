package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySpringBootTopicConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MySpringBootTopicConsumerApplication.class, args);
    }

}
