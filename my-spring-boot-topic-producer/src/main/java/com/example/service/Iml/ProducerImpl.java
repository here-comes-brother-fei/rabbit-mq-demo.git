package com.example.service.Iml;

import com.example.service.Producer;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @BelongsProject: RabbitMqLearn
 * @BelongsPackage: com.example.service.Iml
 * @Author: wang fei
 * @CreateTime: 2023-02-06  17:52
 * @Description: TODO
 * @Version: 1.0
 */
@Service
public class ProducerImpl implements Producer {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Override
    public String sendmessag(String msg) {
        CorrelationData correlationData = new CorrelationData();
        correlationData.setId(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend("my_boot_topic_exchange","product.add", msg,correlationData);
        return "success:"+msg;
    }
}
