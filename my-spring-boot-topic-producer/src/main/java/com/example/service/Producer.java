package com.example.service;

/**
 * @BelongsProject: RabbitMqLearn
 * @BelongsPackage: com.example.service
 * @Author: wang fei
 * @CreateTime: 2023-02-06  17:50
 * @Description: TODO
 * @Version: 1.0
 */
public interface Producer {
    String sendmessag(String msg);
}
