package com.example.controller;

import com.example.service.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @BelongsProject: RabbitMqLearn
 * @BelongsPackage: com.example.controller
 * @Author: wang fei
 * @CreateTime: 2023-02-06  17:55
 * @Description: TODO
 * @Version: 1.0
 */
@RestController
public class ProducerController {
    @Autowired
    private Producer producer;

    @GetMapping("/send")
    public String send(String msg){
        return producer.sendmessag(msg);
    }
}
