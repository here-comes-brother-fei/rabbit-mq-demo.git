package com.example;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MySpringBootTopicProducerApplicationTests {
    @Autowired
    RabbitTemplate rabbitTemplate;

    @Test
    void contextLoads() {
        String msg="Hello spring boot rabbit mq-topic";
        rabbitTemplate.convertAndSend("my_boot_topic_exchange","product.add", msg);
        System.out.println("发送信息成功");
    }
}
