package com.wang.config;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @BelongsProject: RabbitMqLearn
 * @BelongsPackage: com.wang.config
 * @Author: wang fei
 * @CreateTime: 2023-02-04  17:30
 * @Description: TODO
 * @Version: 1.0
 */
@Configuration
public class RabbitConfig {
    private static String EXCHANGE_NAME = "my_boot_fanout_exchange";

    /**
     * 声明交换机
     */
    @Bean
    public FanoutExchange exchange(){
        return new FanoutExchange(EXCHANGE_NAME,true,false);
    }

}
