package com.wang;

import org.junit.After;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MySpringBootProdcerApplicationTests {
    @Autowired
    RabbitTemplate rabbitTemplate;

    @Test
    void contextLoads() {
    String msg="Hello spring boot rabbit mq";
    rabbitTemplate.convertAndSend("my_boot_fanout_exchange","", msg);
    }
}
