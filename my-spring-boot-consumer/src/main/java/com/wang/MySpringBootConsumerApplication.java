package com.wang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 飞
 */
@SpringBootApplication
public class MySpringBootConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MySpringBootConsumerApplication.class, args);
    }

}
