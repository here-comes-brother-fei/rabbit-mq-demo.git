package com.wang.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @BelongsProject: RabbitMqLearn
 * @BelongsPackage: com.wang.com.wang.config
 * @Author: wang fei
 * @CreateTime: 2023-02-04  17:08
 * @Description: TODO 声明队列、交换机、绑定关系
 * @Version: 1.0
 */
@Configuration
public class RabbitConfig {


    private static String QUEUE_NAME ="my_boot_fanout_queue";
    private static String EXCHANGE_NAME = "my_boot_fanout_exchange";

    /**
     * @description: 声明队列
     * @method: queue
     * @author: wang fei
     * @date: 2023/2/4 17:14:03
     * @param: []
     * @return: org.springframework.amqp.core.Queue
    **/
    @Bean
    public Queue queue(){
        return  new Queue(QUEUE_NAME,true,false,false);
    }

    /**
     * @description: 声明交换机
     * @method: exchange
     * @author: wang fei
     * @date: 2023/2/4 17:14:54
     * @param: []
     * @return: org.springframework.amqp.core.FanoutExchange
    **/
    @Bean
    public FanoutExchange exchange(){
        return   new FanoutExchange(EXCHANGE_NAME,true,false);
    }

    /**
     * @description: 声明绑定关系 注意：后⾯多了with携带routing-key
     * @method: binding
     * @author: wang fei
     * @date: 2023/2/4 17:17:04
     * @param: [queue, exchange]
     * @return: org.springframework.amqp.core.Binding
    **/
    @Bean
    public Binding binding(Queue queue, FanoutExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange);
    }
}
