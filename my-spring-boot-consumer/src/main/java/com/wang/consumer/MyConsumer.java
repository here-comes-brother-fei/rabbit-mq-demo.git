package com.wang.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: RabbitMqLearn
 * @BelongsPackage: com.wang.config.consumer
 * @Author: wang fei
 * @CreateTime: 2023-02-04  17:20
 * @Description: TODO MQ 订阅者
 * @Version: 1.0
 */
@Component
public class MyConsumer {

    /**
     * @description: 监听队列，当队列中有消息的时候，该⽅法会被回调，⽤来消费消息
     * @method: receive
     * @author: wang fei
     * @date: 2023/2/4 17:22:35
     * @param: [message]
     * @return: void
    **/
    @RabbitListener(queues = "my_boot_fanout_queue")
    public void receive(String message){
        byte[] msg = message.getBytes();
        System.out.println("收到消息：" + new String(msg));
    }
}
